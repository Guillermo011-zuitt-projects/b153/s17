// parseint() - to convert data type to number

let grades = [98.5, 94.3, 89.2, 90.1];

let z = grades.length;
let newValue = 100;
grades[z] = newValue;

console.log(grades);

grades.push(80.8);

console.log(grades);

// push() - add element at the end
// pop() - removed element at the end but hold it
// unshift() - add element in the beginning of array
// shift() - removes array's first element and returns the removed element
// splice() can add and remove from anywhere in an array, and even do both at once (syntax: .splice(starting index number, number of items to remove, items to add)

let tasks = ['shower', 'eat breakfast', 'go to work', 'go home', 'go to sleep'];
// add with splice
tasks.splice(3,0,"eat lunch");
console.log(tasks);
// remove with splice
tasks.splice(2,1);
console.log(tasks);

// sort() - rearranges elements in alphanumeric order
// reverse() - reverses the order of your array elements

let a = [1, 2, 3, 4, 5];
let b = [6, 7, 8, 9, 10];

// concat()

let ab = a.concat(b);

console.log(ab);

// join() - will take all the elements and convert them into a string
// whatever you put inside the join(), it will become the separator
let joinedArray = ab.join(" ");

console.log(joinedArray);

// slice() - copies specified array elements into a new array
//  - slice(starting array point, ending array point) it does not include the end array index
let c = a.slice(0, 3);
console.log(c);

// Mini Activity

let numbersArr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
let placeholderArr = [];

for (i = 0; i < numbersArr.length; i++){
	if (numbersArr[i]%2 === 0){
		placeholderArr.push(numbersArr[i]);
	}
}
console.log(placeholderArr);

// Array Iteration Methods
// JavaScript loops only for arrays
// .foreach() - same as looping but just for an arrays
// uses an anonymous function(means function without a name) inside the foreach parenthesis


// map() - creates a new array populated with the results of running each element of the mapped array

let numbersArr2 = [1, 2, 3, 4, 5];

let doubledNumbers = numbersArr2.map(function(number){
	return number * 2
})
console.log(doubledNumbers);

// every() - will check every element in an array and see if they all match the given condition (returns true or false)

let allValid = numbersArr2.every(function(number){
	return (typeof number === 'number');
})
console.log(allValid);

// some() - will check every element in an array and see if some match the given condition (returns true or false)

let someValid = numbersArr2.some(function(number){
	return number < 3;
})

console.log(someValid);

// filter() - creates a new array only populated with elements that match the given condition

let words = ['spray', 'limit', 'elite', 'exuberant', 'destruction', 'present'];

let wordsResult = words.filter(function(words){
	return words.length > 6;
})

console.log(wordsResult);

// includes() - checks if the specified value is included in the array (returns true or false)

console.log(words.includes('limit'));


// Two-Dimensional Arrays (Multi-Dimensional Arrays)
// - you can have a nested arrays

let twoDimensional = [
	[1.1, 1.2, 1.3],
	[2.1, 2.2, 2.3],
	[3.1, 3.2, 3.3]
]
console.log(twoDimensional[1]); // just to get the array inside the array
// how to get an element inside the array, just works like coordinates

console.log(twoDimensional[0][1]);




let studentArr = ['john', 'jane', 'mary'];

	let addSection = studentArr.map(function(element){
		return element + " - Section ";

	})	
		console.log(addSection);


// indexof() - to locate the index of the element inside the array