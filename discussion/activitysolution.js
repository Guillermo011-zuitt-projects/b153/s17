let students = []

console.log(students)

function addStudent(name){
	students.push(name)
	console.log(name + " was added to the students list.")
}

function countStudents(){
	console.log("There are a total of " + students.length + " students enrolled.")
}

function printStudents(){
	students.sort()

	students.forEach(function(student){
		console.log(student)
	})
}


function findStudent(keyword){

	// keyword = keyword.toLowerCase()

	// let lowerCaseNames = students.map(function(student){
	// 	return student.toLowerCase()
	// })

	// if(lowerCaseNames.includes(keyword)){
	// 	console.log(keyword + " is an enrollee.")
	// }else{
	// 	console.log(keyword + " is NOT an enrollee.")
	// }

	let matches = students.filter(function(student){
		return student.toLowerCase().includes(keyword.toLowerCase())
	})

	if(matches.length === 1){
		console.log(matches + " is an enrollee.")
	}else if(matches.length > 1){
		matches = matches.join(", ")
		console.log(matches + " are enrollees.")
	}else{
		console.log("No student found with the name " + keyword)
	}

}

function addSection(section){

	let studentSections = students.map(function(student){
		return student + ' - Section ' + section
	})

	console.log(studentSections)
}

function removeStudent(studentToRemove){
	// student = student.toLowerCase()

	// // console.log(student)

	// let lowerCaseNames = students.map(function(student){
	// 	return student.toLowerCase()
	// })

	// let studentIndex = lowerCaseNames.indexOf(student)

	// lowerCaseNames.splice(studentIndex, 1)

	// console.log(student + " was removed from the students list.")

	// students = lowerCaseNames

	students = students.filter(function(student){
		return student.toLowerCase() !== studentToRemove.toLowerCase()
	})

	console.log(studentToRemove + " was removed from the students list.")
}
